# Локальная сборка образа

Собираем приложение в файл JAR с помощью команды maven:
$ mvn clean package

Запускаем сборку образа:
$ docker build -t koudelnypa/skillbox-microservice-users:1.0.0 .

Для сборки через spring-boot-maven-plugin:
$ mvn spring-boot:build-image

Сохранение docker-образа на docker hub:
$ docker push koudelnypa/skillbox-microservice-users:1.0.1


# Запуск БД и приложения в отдельных контейнерах

Просмотр списка сетей docker:
$ docker network ls

Запуск БД в контейнере:
$ cd docker_run
$ start-db.cmd

Запуск приложения в отдельном контейнере:
$ docker run --rm -p 8080:8080 --network=docker_run_main -e USERS_DATASOURCE_URL="jdbc:postgresql://postgres:5432/skillbox_demo?currentSchema=service_users" koudelnypa/skillbox-microservice-users:1.0.1


# Запуск БД и приложения вместе через docker-compose:

Запуск:
$ cd docker_run
$ start.cmd

# Разворачивание в minikube
Запустить minikube:
$ minikube start

Создать пространство имен:
$ kubectl create ns demo

Посмотреть список пространств имен:
$ kubectl get ns

Установить deploy для сервиса users:
$ kubectl -n demo apply -f .mykuber/pg-deployment.yaml
$ kubectl -n demo apply -f .mykuber/users-Deployment.yaml

Проверить:
$ kubectl -n demo get deploy
$ kubectl -n demo get pods -o wide

Пробрасываем порт от pod-а на хост-машину:
$ kubectl -n demo get pods
$ kubectl -n demo port-forward <имя_пода> 9090:8080

Посмотреть лог пода:
$ kubectl -n demo logs <имя_пода>

Остановить/удалить  minikube:
$ minikube stop
$ minikube delete 