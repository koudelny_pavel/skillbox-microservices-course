------------------------------------------------------------------
-- Создание схемы 
------------------------------------------------------------------

-- DROP SCHEMA service_users;

CREATE SCHEMA IF NOT exists service_users AUTHORIZATION app_user;

------------------------------------------------------------------
-- Таблица users (Пользователи) 
------------------------------------------------------------------

CREATE TABLE IF NOT exists service_users.users  (
	id serial NOT NULL,
	first_name varchar(120) NULL,
	last_name varchar(120) NULL,
	patronymic varchar(120) NULL,
	gender character(1) NULL,
	birthday date NULL,
	city varchar(100) NULL,
	avatar_pic varchar(2000) NULL,
	user_info text NULL,
	nickname varchar(150) NOT NULL,
	email varchar(150) NOT NULL,
	phone varchar(20) NOT NULL,
	creation_time timestamp NULL,
    delete_mark timestamp NULL,
	CONSTRAINT users_pk PRIMARY KEY (id),
	CONSTRAINT users_nickname_uk UNIQUE (nickname, delete_mark),
	CONSTRAINT users_email_uk UNIQUE (email, delete_mark),
	CONSTRAINT users_phone_uk UNIQUE (phone, delete_mark),
	CONSTRAINT users_fio_and_bd_uk UNIQUE (first_name,last_name,patronymic,birthday, delete_mark),
	CONSTRAINT users_gender_check CHECK (gender in ('M', 'F'))
);

CREATE INDEX IF NOT exists users_city_idx ON service_users.users (city);
CREATE INDEX IF NOT exists users_birthday_idx ON service_users.users (birthday);
CREATE INDEX IF NOT exists users_last_name_idx ON service_users.users (last_name);
CREATE INDEX IF NOT exists users_gender_idx ON service_users.users using HASH (gender);

COMMENT ON TABLE service_users.users IS 'Пользователи';

-- Column comments

COMMENT ON COLUMN service_users.users.id IS 'Id пользователя';
COMMENT ON COLUMN service_users.users.first_name IS 'Имя пользователя';
COMMENT ON COLUMN service_users.users.last_name IS 'Фамилия';
COMMENT ON COLUMN service_users.users.patronymic IS 'Отчество';
COMMENT ON COLUMN service_users.users.gender IS 'Пол';
COMMENT ON COLUMN service_users.users.birthday IS 'Дата рождения';
COMMENT ON COLUMN service_users.users.city IS 'Город проживания';
COMMENT ON COLUMN service_users.users.avatar_pic IS 'Картинка-аватар';
COMMENT ON COLUMN service_users.users.user_info IS 'Информация о пользователе (небольшое поле о себе)';
COMMENT ON COLUMN service_users.users.nickname IS 'Никнейм пользователя';
COMMENT ON COLUMN service_users.users.email IS 'Электронная почта';
COMMENT ON COLUMN service_users.users.phone IS 'Телефон';
COMMENT ON COLUMN service_users.users.delete_mark IS 'Признак удаления записи';

------------------------------------------------------------------
-- Таблица skills (Навыки) 
------------------------------------------------------------------

CREATE TABLE IF NOT exists service_users.skills (
	id serial NOT NULL,
	skill_name varchar(150) NOT NULL,
	CONSTRAINT skills_pk PRIMARY KEY (id),
	CONSTRAINT skills_name_uk UNIQUE (skill_name)
);
COMMENT ON TABLE service_users.skills IS 'Навыки';

-- Column comments

COMMENT ON COLUMN service_users.skills.id IS 'Id навыка';
COMMENT ON COLUMN service_users.skills.skill_name IS 'Название навыка';

------------------------------------------------------------------
-- Таблица user_skills (Навыки пользователей) 
------------------------------------------------------------------

CREATE TABLE IF NOT exists service_users.user_skills (
    id serial NOT NULL,
	user_id bigint NOT NULL,
	skill_id bigint NOT NULL,
	CONSTRAINT user_skills_pk PRIMARY KEY (id),
	CONSTRAINT user_skills_uk UNIQUE (user_id, skill_id),
	CONSTRAINT user_skills_user_fk FOREIGN KEY (user_id) REFERENCES service_users.users(id) ON DELETE CASCADE,
	CONSTRAINT user_skills_skill_fk FOREIGN KEY (skill_id) REFERENCES service_users.skills(id) ON DELETE CASCADE
);

CREATE INDEX IF NOT exists user_skills_user_id_idx ON service_users.user_skills (user_id);
CREATE INDEX IF NOT exists user_skills_skill_id_idx ON service_users.user_skills (skill_id);

COMMENT ON TABLE service_users.user_skills IS 'Навыки пользователей';

-- Column comments

COMMENT ON COLUMN service_users.user_skills.id IS 'Уникальный номер записи';
COMMENT ON COLUMN service_users.user_skills.user_id IS 'Ссылка на пользователя';
COMMENT ON COLUMN service_users.user_skills.skill_id IS 'Ссылка на навык';

------------------------------------------------------------------
-- Таблица user_subscriptions (Подписки пользователя) 
------------------------------------------------------------------

CREATE TABLE IF NOT exists service_users.user_subscriptions (
	id serial NOT NULL,
	subscriber_user_id bigint NOT NULL,
	publisher_user_id bigint NOT NULL,
	CONSTRAINT user_subscriptions_pk PRIMARY KEY (id),
	CONSTRAINT user_subscriptions_uk UNIQUE (subscriber_user_id,publisher_user_id),
	CONSTRAINT user_subscriptions_subs_fk FOREIGN KEY (subscriber_user_id) REFERENCES service_users.users(id) ON DELETE CASCADE,
	CONSTRAINT user_subscriptions_pub_fk FOREIGN KEY (publisher_user_id) REFERENCES service_users.users(id) ON DELETE CASCADE
);

CREATE INDEX IF NOT exists user_subs_s_user_id_idx ON service_users.user_subscriptions (subscriber_user_id);
CREATE INDEX IF NOT exists user_subs_p_user_id_idx ON service_users.user_subscriptions (publisher_user_id);

COMMENT ON TABLE service_users.user_subscriptions IS 'Подписки пользователя';

-- Column comments

COMMENT ON COLUMN service_users.user_subscriptions.id IS 'Уникальный номер записи';
COMMENT ON COLUMN service_users.user_subscriptions.subscriber_user_id IS 'Пользователь-подписчик';
COMMENT ON COLUMN service_users.user_subscriptions.publisher_user_id IS 'Пользователь-издатель';

