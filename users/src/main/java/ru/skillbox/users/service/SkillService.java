package ru.skillbox.users.service;

import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.users.entity.Skill;
import ru.skillbox.users.repository.SkillRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SkillService {
    private final SkillRepository skillRepository;
    private final static Logger log = LoggerFactory.getLogger(SkillService.class);

    public SkillService(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Transactional
    public void createSkill(Skill skill) {
        Optional<Skill> foundSkill = skillRepository.findBySkillName(skill.getSkillName());
        if(foundSkill.isPresent()){
            log.warn("Навык {} уже существует (id = {})",
                    skill.getSkillName(), foundSkill.get().getId());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        Skill savedSkill = skillRepository.save(skill);
        log.info("Навык {} сохранен в БД с id = {}", skill.getSkillName(), savedSkill.getId());
    }

    public Skill getSkill(long id) {
        return skillRepository.findById(id).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Transactional
    public void updateSkill(Skill skill, long id) {
        if(!skillRepository.existsById(id)){
            log.warn("Навык с id = {} не найден в БД", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(skill.getId() == null || !skill.getId().equals(id)) {
            log.warn("Ошибка передачи параметров: {} <> {}", skill.getId(), id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        skillRepository.save(skill);
        log.info("Изменения для навыка {} с id = {} сохранены в БД", skill.getSkillName(), skill.getId());
    }

    @Transactional
    public void deleteSkill(long id) {
        if(!skillRepository.existsById(id)){
            log.warn("Навык с id = {} не найден в БД", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        skillRepository.deleteById(id);
        log.info("Навык с id = {} удален из БД", id);
    }

    public List<Skill> getAllSkills() {
        return skillRepository.findAll();
    }

//    // Возвращает пользователей, имеющих этот навык
//    public Set<User> getSkillUsers(long skillId) {
//        Optional<Skill> foundSkill = skillRepository.findById(skillId);
//        if(!foundSkill.isPresent()){
//            System.out.println(String.format("Навык с id = %d не найден в БД", skillId));
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
//        }
//
//        return foundSkill.get().getUsers();
//    }
}
