package ru.skillbox.users.service;

import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.users.entity.User;
import ru.skillbox.users.repository.UserRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final static Logger log = LoggerFactory.getLogger(UserService.class);

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public Long createUser(User user) {
        Optional<User> foundUser;

        // Проверка на уникальность поля nickname
        foundUser = userRepository.findByNickname(user.getNickname());
        if(foundUser.isPresent()){
            log.warn("Пользователь с nickname = {} уже существует (id = {}})",
                    user.getNickname(), foundUser.get().getId());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        // Проверка на уникальность поля email
        foundUser = userRepository.findByEmail(user.getEmail());
        if(foundUser.isPresent()){
            log.warn("Пользователь с email = {} уже существует (id = {})",
                    user.getEmail(), foundUser.get().getId());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        // Проверка на уникальность поля phone
        foundUser = userRepository.findByPhone(user.getPhone());
        if(foundUser.isPresent()){
            log.warn("Пользователь с phone = {} уже существует (id = {})",
                    user.getPhone(), foundUser.get().getId());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        User savedUser = userRepository.save(user);
        log.info("Пользователь {} сохранен в БД с id = {}", savedUser.getNickname(), savedUser.getId());
        return savedUser.getId();
    }

    public User findOneUser(long id) {
        return userRepository.findById(id)
                .orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Transactional
    public void updateUser(User user, long id) {
        if(!userRepository.existsById(id)){
            log.warn("Пользователь с id = {} не найден в БД", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(user.getId() == null || !user.getId().equals(id)) {
            log.warn("Ошибка передачи параметров: {} <> {}", user.getId(), id);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        userRepository.save(user);
        log.info("Изменения для пользователя {} с id = {} успешно сохранены в БД",
                user.getNickname(), user.getId());
    }

    @Transactional
    public void deleteUser(long id) {
        if(!userRepository.existsById(id)){
            log.warn("Пользователь с id = {} не найден в БД", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        userRepository.deleteById(id);
        log.info("Пользователь с id = {} успешно удален из БД", id);
    }

    public List<User> getAllUsers() {
        // Преобразовать к списку можно было и так: IterableUtils.toList(userRepository.findAll())
        return userRepository.findAll().stream()
                .sorted(Comparator.comparingLong(User::getId))
                .collect(Collectors.toList());
    }

    // Подписать пользователя на pubNickname
    @Transactional
    public void subscribe(long subsUserId, String pubNickname){
        Optional<User> pubUser = userRepository.findByNickname(pubNickname);
        if(!pubUser.isPresent()){
            log.warn("Пользователь с nickname = {} не найден", pubNickname);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        Optional<User> subsUser = userRepository.findById(subsUserId);
        if(!subsUser.isPresent()){
            log.warn("Пользователь с id = {} не найден в БД", subsUserId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        User user = subsUser.get();
        User publisher = pubUser.get();
        if (user.getSubscribes().contains(publisher)){
            log.warn("Пользователь с id = {}  уже подписан на {}", subsUserId, pubNickname);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        user.subscribe(publisher);
        userRepository.save(user);
    }

    // Отменить подписку пользователя на pubNickname
    @Transactional
    public void unsubscribe(long subsUserId, String pubNickname) {
        Optional<User> pubUser = userRepository.findByNickname(pubNickname);
        if(!pubUser.isPresent()){
            log.warn("Пользователь с nickname = {} не найден", pubNickname);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        Optional<User> subsUser = userRepository.findById(subsUserId);
        if(!subsUser.isPresent()){
            log.warn("Пользователь с id = {} не найден в БД", subsUserId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        User user = subsUser.get();
        User publisher = pubUser.get();
        if (!user.getSubscribes().contains(publisher)){
            log.warn("Пользователь с id = {} не подписан на {}", subsUserId, pubNickname);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        user.unsubscribe(publisher);
        userRepository.save(user);
    }

    // Вернуть подписки указанного пользователя
    public Set<User> getUserSubscribes(long subsUserId) {
        Optional<User> subsUser = userRepository.findById(subsUserId);
        if(!subsUser.isPresent()){
            log.warn("Пользователь с id = {} не найден в БД", subsUserId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return subsUser.get().getSubscribes();
    }

}
