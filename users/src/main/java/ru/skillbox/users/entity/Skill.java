package ru.skillbox.users.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Entity
@Table(name = "skills", uniqueConstraints={@UniqueConstraint(columnNames={"skill_name"})})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Skill implements Serializable {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 150, nullable = false)
    private String skillName;
//    @Getter
//    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @JoinTable(name = "user_skills", joinColumns = {@JoinColumn(name = "skill_id")},
//            inverseJoinColumns = {@JoinColumn(name="user_id")},
//            schema = "service_users")
//    private Set<User> users;
}
