package ru.skillbox.users.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users",
        uniqueConstraints={
                @UniqueConstraint(columnNames={"nickname", "delete_mark"}),
                @UniqueConstraint(columnNames={"email", "delete_mark"}),
                @UniqueConstraint(columnNames={"phone", "delete_mark"}),
                @UniqueConstraint(columnNames={"first_name","last_name",
                        "patronymic","birthday", "delete_mark"})
})
@SQLDelete(sql = "UPDATE users SET delete_mark = now() WHERE id=?")
@Where(clause = "delete_mark is null")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 120)
    private String firstName;
    @Column(length = 120)
    private String lastName;
    @Column(length = 120)
    private String patronymic;
    @Enumerated(EnumType.STRING)
    private GenderType gender;
    private LocalDate birthday;
    @Column(length = 100)
    private String city;
    @Column(length = 2000)
    private String avatarPic;
    private String userInfo;
    @Column(length = 150, nullable = false)
    private String nickname;
    @Column(length = 150, nullable = false)
    private String email;
    @Column(length = 20, nullable = false)
    private String phone;
    @CreationTimestamp
    private LocalDateTime creationTime;
    private LocalDateTime deleteMark;

    @Getter
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "user_skills", joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name="skill_id")})
    private Set<Skill> skills = new HashSet<>();

    // Подписки пользователя
    @Getter
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "user_subscriptions", joinColumns = {@JoinColumn(name = "subscriber_user_id")},
            inverseJoinColumns = {@JoinColumn(name="publisher_user_id")})
    private Set<User> subscribes = new HashSet<>();

    public void addSkill(Skill skill){
        skills.add(skill);
    }

    public void addSkills(Collection<Skill> skillList){
        skills.addAll(skillList);
    }

    public void subscribe(User publisher){
        subscribes.add(publisher);
    }

    public void subscribeAll(Collection<User> publishers){
        subscribes.addAll(publishers);
    }

    public void unsubscribe(User publisher) {
        subscribes.remove(publisher);
    }
}
