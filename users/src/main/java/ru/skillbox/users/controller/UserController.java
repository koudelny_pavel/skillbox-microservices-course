package ru.skillbox.users.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.skillbox.users.entity.User;
import ru.skillbox.users.service.SkillService;
import ru.skillbox.users.service.UserService;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

//    @PostMapping
//    public void createUser(@RequestBody User user) {
//        userService.createUser(user);
//    }
    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody User user) {
        Long userId = userService.createUser(user);
        return new ResponseEntity<>("Успешно создан пользователь с id="+userId, HttpStatus.CREATED);
    }

    @GetMapping(path = "/{id}")
    public User findOneUser(@PathVariable long id) {
        return userService.findOneUser(id);
    }

    @PutMapping(path = "/{id}")
    public void updateUser(@RequestBody User user, @PathVariable long id) {
        userService.updateUser(user, id);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable long id) {
        userService.deleteUser(id);
    }

    @GetMapping
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    // Добавить подписку на pubNickname
    @PutMapping(path = "/{subs_user_id}/subscribe/{pub_nickname}")
    @ResponseStatus(HttpStatus.OK)
    public void subscribe(@PathVariable(name = "subs_user_id") long subsUserId,
                          @PathVariable(name = "pub_nickname") String pubNickname) {
        userService.subscribe(subsUserId, pubNickname);
    }

    // Отменить подписку на pubNickname
    @DeleteMapping(path = "/{subs_user_id}/subscribe/{pub_nickname}")
    @ResponseStatus(HttpStatus.OK)
    public void unsubscribe(@PathVariable(name = "subs_user_id") long subsUserId,
                            @PathVariable(name = "pub_nickname") String pubNickname) {
        userService.unsubscribe(subsUserId, pubNickname);
    }

    // Получить все подписки пользователя
    @GetMapping(path = "/{subs_user_id}/subscribes")
    public Set<User> getUserSubscribes(@PathVariable(name = "subs_user_id") long subsUserId) {
        return userService.getUserSubscribes(subsUserId);
    }

}
