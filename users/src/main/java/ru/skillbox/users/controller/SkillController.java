package ru.skillbox.users.controller;

import org.springframework.web.bind.annotation.*;
import ru.skillbox.users.entity.Skill;
import ru.skillbox.users.service.SkillService;

import java.util.List;

@RestController
@RequestMapping(value = "/skills")
public class SkillController {
    private final SkillService skillService;

    public SkillController(SkillService skillService) {
        this.skillService = skillService;
    }

    @PostMapping
    public void createSkill(@RequestBody Skill skill) {
        skillService.createSkill(skill);
    }

    @GetMapping(path = "/{id}")
    public Skill getUser(@PathVariable long id) {
        return skillService.getSkill(id);
    }

    @PutMapping(path = "/{id}")
    public void updateSkill(@RequestBody Skill skill, @PathVariable long id) {
        skillService.updateSkill(skill, id);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteSkill(@PathVariable long id) {
        skillService.deleteSkill(id);
    }

    @GetMapping
    public List<Skill> getAllSkills() {
        return skillService.getAllSkills();
    }

//    // Все пользователи с таким навыком
//    @GetMapping(path = "/{skill_id}/users")
//    public Set<User> getUsersWithSkill(@PathVariable(name = "skill_id") long skillId) {
//        return skillService.getSkillUsers(skillId);
//    }
}
