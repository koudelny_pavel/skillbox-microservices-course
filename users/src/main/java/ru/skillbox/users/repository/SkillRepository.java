package ru.skillbox.users.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.skillbox.users.entity.Skill;
import ru.skillbox.users.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface SkillRepository extends CrudRepository<Skill,Long> {
    public List<Skill> findAll();
    public Optional<Skill> findBySkillName(String skillName);
}
