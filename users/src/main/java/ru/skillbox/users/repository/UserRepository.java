package ru.skillbox.users.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.skillbox.users.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {
    public List<User> findAll();
    public Optional<User> findByNickname(String nickname);
    public Optional<User> findByEmail(String email);
    public Optional<User> findByPhone(String phone);
}
