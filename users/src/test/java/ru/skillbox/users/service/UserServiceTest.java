package ru.skillbox.users.service;

import jakarta.persistence.PersistenceException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.users.entity.GenderType;
import ru.skillbox.users.entity.User;
import ru.skillbox.users.repository.UserRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    private static final long EXISTING_USER_ID = 1L;
    private static final long TEST_USER_ID = 2L;
    private static final long NOT_EXISTING_USER_ID = 3L;
    @Mock
    UserRepository userRepository;
    private User existingUser;
    private User testUser;
    private User newUser;
    private UserService service;


//    @BeforeAll
//    public void setup() {
//    }
//
//    @AfterAll
//    public void teardown() {
//    }
    @BeforeEach
    void setUp() {
        existingUser = createExistingUser(EXISTING_USER_ID);
        testUser = createTestUser(TEST_USER_ID);
        newUser = createNewUser();
        service = new UserService(userRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @DisplayName("Ошибка. Создания нового пользователя с Nickname существующего польз.")
    void createUserThrowExceptionWhenNicknameExists() {
        Mockito.when(userRepository.findByNickname(existingUser.getNickname()))
                .thenReturn(Optional.of(existingUser));
        //UserService service = new UserService(userRepository);
        newUser.setNickname(existingUser.getNickname());
        Assertions.assertThrows(
                  ResponseStatusException.class
                , () ->  service.createUser(newUser)
                , "ResponseStatusException was expected");
    }

    @Test
    @DisplayName("Ошибка. Создания нового пользователя с email существующего польз.")
    void createUserThrowExceptionWhenEmailExists() {
        Mockito.when(userRepository.findByEmail(existingUser.getEmail()))
                .thenReturn(Optional.of(existingUser));
        newUser.setEmail(existingUser.getEmail());
        Assertions.assertThrows(ResponseStatusException.class
                , () ->  service.createUser(newUser)
                , "ResponseStatusException was expected");
    }

    @Test
    @DisplayName("Ошибка. Создания нового пользователя с телефоном существующего польз.")
    void createUserThrowExceptionWhenPhoneExists() {
        Mockito.when(userRepository.findByPhone(existingUser.getPhone()))
                .thenReturn(Optional.of(existingUser));
        newUser.setPhone(existingUser.getPhone());
        Assertions.assertThrows(ResponseStatusException.class
                , () ->  service.createUser(newUser)
                , "ResponseStatusException was expected");
    }

    @Test
    @DisplayName("Ошибка при ошибке сохранения пользователя в БД")
    void createUserThrowExceptionWhenSave() {
        Mockito.when(userRepository.save(newUser)).thenThrow(PersistenceException.class);
        Executable executable = () -> service.createUser(newUser);
        Assertions.assertThrows(PersistenceException.class
                , executable
                , "PersistenceException was expected");
    }

    @Test
    @DisplayName("Успешное создание нового пользователя")
    void createUserSuccess() {
        Mockito.when(userRepository.save(newUser)).thenReturn(newUser);
        service.createUser(newUser);
        Mockito.verify(userRepository, Mockito.times(1)).save(newUser);
    }

    @Test
    @DisplayName("Ошибка при поиске несуществующего пользователя")
    void getUserThrowExceptionWhenNotFound() {
        ResponseStatusException thrown = Assertions.assertThrows(ResponseStatusException.class
                , () ->  {User foundUser = service.findOneUser(NOT_EXISTING_USER_ID); }
                , "ResponseStatusException was expected");
    }

    @Test
    @DisplayName("Успешный поиск пользователя")
    void getUserSuccess() {
        User existingUser = createExistingUser(1L);
        //UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(existingUser.getId()))
                .thenReturn(Optional.of(existingUser));
        //UserService service = new UserService(userRepository);
        User foundUser = service.findOneUser(existingUser.getId());
        assertTrue(foundUser.getNickname().equals(existingUser.getNickname()),
                "Ожидался, что будет найден пользователь "+existingUser.getNickname());
    }

    @Test
    @DisplayName("Успешное сохранение пользователя")
    void updateUserSuccess() {
        User existingUser = createExistingUser(1L);
        //UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.existsById(existingUser.getId()))
                .thenReturn(true);
        //UserService service = new UserService(userRepository);
        service.updateUser(existingUser, 1L);

        Mockito.verify(userRepository, Mockito.times(1)).save(existingUser);
    }

    @Test
    @DisplayName("Исключение при попытке сохранения пользователя с другим id")
    void updateUserFail() {
        User existingUser = createExistingUser(1L);
        //UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.existsById(2L))
                .thenReturn(true);
        //UserService service = new UserService(userRepository);


        Executable executable = () -> service.updateUser(existingUser, 2L);
        Assertions.assertThrows(ResponseStatusException.class
                , executable
                , "ResponseStatusException was expected");
    }

    @Test
    @DisplayName("Успешное удаление пользователя с указанным id")
    void deleteUserSuccess() {
        User existingUser = createExistingUser(1L);
        //UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.existsById(existingUser.getId()))
                .thenReturn(true);
        //UserService service = new UserService(userRepository);
        service.deleteUser(existingUser.getId());

        Mockito.verify(userRepository, Mockito.times(1))
                .deleteById(existingUser.getId());
    }

    @Test
    @DisplayName("Исключение при попытке удаления несуществующего пользователя")
    void deleteUserFail() {
        //UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.existsById(1L)).thenReturn(false);
        //UserService service = new UserService(userRepository);

        Executable executable = () -> service.deleteUser(1L);
        Assertions.assertThrows(ResponseStatusException.class
                , executable
                , "ResponseStatusException was expected");
    }

//    @Test
//    void getAllUsers() {
//        List<User> expectedUserList = List.of(createExistingUser(null),
//                createTestUser(null));
//        UserRepository userRepository = Mockito.mock(UserRepository.class);
//        Mockito.when(userRepository.findAll())
//                .thenReturn(expectedUserList);
//        UserService service = new UserService(userRepository);
//        List<User> actualUserList = service.getAllUsers();
//
//        assertNotNull(actualUserList);
//        assertTrue(actualUserList.size() == 2);
//        assertEquals(expectedUserList, actualUserList);
//    }

    @Test
    @DisplayName("Успешное добавление пользователя в подписку")
    void subscribeSuccess() {
        Mockito.when(userRepository.findByNickname(existingUser.getNickname()))
                .thenReturn(Optional.of(existingUser));
        Mockito.when(userRepository.findById(testUser.getId()))
                .thenReturn(Optional.of(testUser));
        service.subscribe(testUser.getId(), existingUser.getNickname());
        Mockito.verify(userRepository, Mockito.times(1))
                .save(testUser);
        assertTrue(testUser.getSubscribes().size()==1);
        assertTrue(testUser.getSubscribes().contains(existingUser));
    }

    @Test
    @DisplayName("Успешное отмена подписки пользователя")
    void unsubscribeSuccess() {
        testUser.subscribe(existingUser);
        Mockito.when(userRepository.findByNickname(existingUser.getNickname()))
                .thenReturn(Optional.of(existingUser));
        Mockito.when(userRepository.findById(testUser.getId()))
                .thenReturn(Optional.of(testUser));
        service.unsubscribe(testUser.getId(), existingUser.getNickname());
        Mockito.verify(userRepository, Mockito.times(1))
                .save(testUser);
        assertTrue(testUser.getSubscribes().size()==0);
    }

    @Test
    @DisplayName("Получение списка всех пользователей")
    void getAllUsers() {
        Mockito.when(userRepository.findAll()).thenReturn(List.of(existingUser));
        List<User> allUserList = service.getAllUsers();
        Mockito.verify(userRepository, Mockito.times(1)).findAll();
        assertTrue(allUserList.size()==1);
        assertTrue(allUserList.get(0)==existingUser);
    }

    public static User createExistingUser(Long userId){
        User existingUser = new User();
        existingUser.setId(userId);
        existingUser.setFirstName("Existing");
        existingUser.setLastName("User");
        existingUser.setGender(GenderType.M);
        existingUser.setNickname("ExistingUserNickname");
        existingUser.setEmail("existing_user@test.ru");
        existingUser.setPhone("111-11-11");
        return existingUser;
    }

    public static User cloneUser(User user, Long newUserId){
        User newUser = new User();
        newUser.setId(newUserId);
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setGender(user.getGender());
        newUser.setNickname(user.getNickname());
        newUser.setEmail(user.getEmail());
        newUser.setPhone(user.getPhone());
        //user.setSkills(skillList);
        return newUser;
    }

    public static User createTestUser(Long testUserId){
        User testUser = new User();
        testUser.setId(testUserId);
        testUser.setFirstName("Test");
        testUser.setLastName("User");
        testUser.setGender(GenderType.M);
        testUser.setNickname("TestUserNickname");
        testUser.setEmail("user@test.ru");
        testUser.setPhone("0980-09-03");
        //user.setSkills(skillList);
        return testUser;
    }

    public static User createNewUser(){
        User newUser = new User();
        newUser.setFirstName("New");
        newUser.setLastName("User");
        newUser.setGender(GenderType.M);
        newUser.setNickname("NewUserNickname");
        newUser.setEmail("new-user@test.ru");
        newUser.setPhone("0980-09-03");
        //user.setSkills(skillList);
        return newUser;
    }


}